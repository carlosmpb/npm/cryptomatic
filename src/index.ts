import crypto from 'crypto'


export const randomBytes = crypto.randomBytes

export const randomString = function(size:number):string {
  let str:string
  do  {
    str = crypto.randomBytes(size).toString('base64').replace(/[\+\/\=]/g, '').slice(0, size)
  } while (str.length !== size)
  return str
}

export const randomNumberString = function(size:number):string {
  const str:Array<string> = []
  for (let i:number=0; i<size; i++) {
    str[i] = crypto.randomInt(0,9).toString()
  }
  return str.join('')
}

export const md5 = function(input:string, salt?:string):string {

  if ('string' === typeof salt) {
    input = input + salt
  }
  const output:string = crypto
    .createHash('md5')
    .update(input)
    .digest('hex')

  return output
}

export const shortMD5 = function(input:string, size:number = 10, salt?:string):string {

  if ('string' === typeof salt) {
    input = input + salt
  }
  const output:string = crypto
    .createHash('md5')
    .update(input)
    .digest('base64')

  return output.replace(/[\+\/\=]/g, '').slice(0, Math.max(1, Math.min(16, size)))
}

export const sha256 = function(input:string, salt?:string):string {

  if ('string' === typeof salt) {
    input = input + salt
  }
  const output:string = crypto
    .createHash('sha256')
    .update(input)
    .digest('hex')

  return output
}

export const shortSHA256 = function(input:string, size:number = 10, salt?:string):string {

  if ('string' === typeof salt) {
    input = input + salt
  }
  const output:string = crypto
    .createHash('sha256')
    .update(input)
    .digest('base64')

  return output.replace(/[\+\/\=]/g, '').slice(0, Math.max(1, Math.min(16, size)))
}

export const hashPassword = function(password:string, salt?:string, {
  pepper = '',
  digest = 'sha256',
  keylen = 32,
  iterations = 500000
} = {}):{ hash:string, salt:string } {

  if ('string' !== typeof salt) {

    salt = randomString(keylen)
  }

  const derivedKey = crypto.pbkdf2Sync(
    password,
    salt + pepper,
    iterations,
    keylen,
    digest
  )

  return { hash: derivedKey.toString('hex'), salt }
}

export const AES = {

  cbc: {
    algorithm: 'aes-256-cbc',
    ivLength: 16,

    encrypt: function(input:string, secret:string):string {
      const iv = crypto.randomBytes(AES.cbc.ivLength)
      const cipher = crypto.createCipheriv(
        AES.cbc.algorithm,
        secret,
        iv
      )
      let encrypted = cipher.update(input)
      encrypted = Buffer.concat([encrypted, cipher.final()])
      return Buffer.concat([iv, encrypted]).toString('base64')
    },

    decrypt: function(strInput:string, secret:string):string {
      const input = Buffer.from(strInput, 'base64')
      const iv = Uint8Array.prototype.slice.call(input, 0, AES.cbc.ivLength)
      const encrypted = Uint8Array.prototype.slice.call(input, AES.cbc.ivLength)
      const decipher = crypto.createDecipheriv(
        AES.cbc.algorithm,
        secret,
        iv
      )
      let decrypted = decipher.update(encrypted)
      decrypted = Buffer.concat([decrypted, decipher.final()])
      return decrypted.toString()
    }
  },

  gcm: {
    algorithm: 'aes-256-gcm',
    authTagLength: 16,
    ivLength: 12,

    encrypt: function(input:string, secret:string):string {
      const iv = crypto.randomBytes(AES.gcm.ivLength)
      const cipher = crypto.createCipheriv(
        AES.gcm.algorithm as crypto.CipherCCMTypes,
        secret,
        iv,
        { authTagLength: AES.gcm.authTagLength }
      )
      let encrypted = cipher.update(input)
      encrypted = Buffer.concat([encrypted, cipher.final()])
      return Buffer.concat([iv, encrypted, cipher.getAuthTag()]).toString('base64')
    },

    decrypt: function(strInput:string, secret:string):string {
      const input = Buffer.from(strInput, 'base64')
      const authTag = Uint8Array.prototype.slice.call(input, -AES.gcm.authTagLength)
      const iv = Uint8Array.prototype.slice.call(input, 0, AES.gcm.ivLength)
      const encrypted = Uint8Array.prototype.slice.call(input, AES.gcm.ivLength, -AES.gcm.authTagLength)
      const decipher = crypto.createDecipheriv(
        AES.gcm.algorithm as crypto.CipherCCMTypes,
        secret,
        iv,
        { authTagLength: AES.gcm.authTagLength }
      )
      decipher.setAuthTag(authTag)
      let decrypted = decipher.update(encrypted)
      decrypted = Buffer.concat([decrypted, decipher.final()])
      return decrypted.toString()
    }
  }
}
