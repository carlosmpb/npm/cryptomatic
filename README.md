# cryptomatic

Helper library for easy AES encrypt/decrypt, hashing and secure random string generation built on top of Node.js built-in crypto module

## Installation

This is a [Node.js](https://nodejs.org/) module available through the 
[npm registry](https://www.npmjs.com/). It can be installed using the 
[`npm`](https://docs.npmjs.com/getting-started/installing-npm-packages-locally)
or 
[`yarn`](https://yarnpkg.com/en/)
command line tools.

```sh
npm install cryptomatic --save
```

## Dependencies

None

## License

ISC
