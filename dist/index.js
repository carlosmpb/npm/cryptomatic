"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AES = exports.hashPassword = exports.shortSHA256 = exports.sha256 = exports.shortMD5 = exports.md5 = exports.randomNumberString = exports.randomString = exports.randomBytes = void 0;
const crypto_1 = __importDefault(require("crypto"));
exports.randomBytes = crypto_1.default.randomBytes;
const randomString = function (size) {
    let str;
    do {
        str = crypto_1.default.randomBytes(size).toString('base64').replace(/[\+\/\=]/g, '').slice(0, size);
    } while (str.length !== size);
    return str;
};
exports.randomString = randomString;
const randomNumberString = function (size) {
    const str = [];
    for (let i = 0; i < size; i++) {
        str[i] = crypto_1.default.randomInt(0, 9).toString();
    }
    return str.join('');
};
exports.randomNumberString = randomNumberString;
const md5 = function (input, salt) {
    if ('string' === typeof salt) {
        input = input + salt;
    }
    const output = crypto_1.default
        .createHash('md5')
        .update(input)
        .digest('hex');
    return output;
};
exports.md5 = md5;
const shortMD5 = function (input, size = 10, salt) {
    if ('string' === typeof salt) {
        input = input + salt;
    }
    const output = crypto_1.default
        .createHash('md5')
        .update(input)
        .digest('base64');
    return output.replace(/[\+\/\=]/g, '').slice(0, Math.max(1, Math.min(16, size)));
};
exports.shortMD5 = shortMD5;
const sha256 = function (input, salt) {
    if ('string' === typeof salt) {
        input = input + salt;
    }
    const output = crypto_1.default
        .createHash('sha256')
        .update(input)
        .digest('hex');
    return output;
};
exports.sha256 = sha256;
const shortSHA256 = function (input, size = 10, salt) {
    if ('string' === typeof salt) {
        input = input + salt;
    }
    const output = crypto_1.default
        .createHash('sha256')
        .update(input)
        .digest('base64');
    return output.replace(/[\+\/\=]/g, '').slice(0, Math.max(1, Math.min(16, size)));
};
exports.shortSHA256 = shortSHA256;
const hashPassword = function (password, salt, { pepper = '', digest = 'sha256', keylen = 32, iterations = 500000 } = {}) {
    if ('string' !== typeof salt) {
        salt = (0, exports.randomString)(keylen);
    }
    const derivedKey = crypto_1.default.pbkdf2Sync(password, salt + pepper, iterations, keylen, digest);
    return { hash: derivedKey.toString('hex'), salt };
};
exports.hashPassword = hashPassword;
exports.AES = {
    cbc: {
        algorithm: 'aes-256-cbc',
        ivLength: 16,
        encrypt: function (input, secret) {
            const iv = crypto_1.default.randomBytes(exports.AES.cbc.ivLength);
            const cipher = crypto_1.default.createCipheriv(exports.AES.cbc.algorithm, secret, iv);
            let encrypted = cipher.update(input);
            encrypted = Buffer.concat([encrypted, cipher.final()]);
            return Buffer.concat([iv, encrypted]).toString('base64');
        },
        decrypt: function (strInput, secret) {
            const input = Buffer.from(strInput, 'base64');
            const iv = Uint8Array.prototype.slice.call(input, 0, exports.AES.cbc.ivLength);
            const encrypted = Uint8Array.prototype.slice.call(input, exports.AES.cbc.ivLength);
            const decipher = crypto_1.default.createDecipheriv(exports.AES.cbc.algorithm, secret, iv);
            let decrypted = decipher.update(encrypted);
            decrypted = Buffer.concat([decrypted, decipher.final()]);
            return decrypted.toString();
        }
    },
    gcm: {
        algorithm: 'aes-256-gcm',
        authTagLength: 16,
        ivLength: 12,
        encrypt: function (input, secret) {
            const iv = crypto_1.default.randomBytes(exports.AES.gcm.ivLength);
            const cipher = crypto_1.default.createCipheriv(exports.AES.gcm.algorithm, secret, iv, { authTagLength: exports.AES.gcm.authTagLength });
            let encrypted = cipher.update(input);
            encrypted = Buffer.concat([encrypted, cipher.final()]);
            return Buffer.concat([iv, encrypted, cipher.getAuthTag()]).toString('base64');
        },
        decrypt: function (strInput, secret) {
            const input = Buffer.from(strInput, 'base64');
            const authTag = Uint8Array.prototype.slice.call(input, -exports.AES.gcm.authTagLength);
            const iv = Uint8Array.prototype.slice.call(input, 0, exports.AES.gcm.ivLength);
            const encrypted = Uint8Array.prototype.slice.call(input, exports.AES.gcm.ivLength, -exports.AES.gcm.authTagLength);
            const decipher = crypto_1.default.createDecipheriv(exports.AES.gcm.algorithm, secret, iv, { authTagLength: exports.AES.gcm.authTagLength });
            decipher.setAuthTag(authTag);
            let decrypted = decipher.update(encrypted);
            decrypted = Buffer.concat([decrypted, decipher.final()]);
            return decrypted.toString();
        }
    }
};
