/// <reference types="node" />
import crypto from 'crypto';
export declare const randomBytes: typeof crypto.randomBytes;
export declare const randomString: (size: number) => string;
export declare const randomNumberString: (size: number) => string;
export declare const md5: (input: string, salt: string) => string;
export declare const shortMD5: (input: string, size: number, salt: string) => string;
export declare const sha256: (input: string, salt: string) => string;
export declare const shortSHA256: (input: string, size: number, salt: string) => string;
export declare const hashPassword: (password: string, salt: string, { pepper, digest, keylen, iterations }?: {
    pepper?: string;
    digest?: string;
    keylen?: number;
    iterations?: number;
}) => {
    hash: string;
    salt: string;
};
export declare const AES: {
    cbc: {
        algorithm: string;
        ivLength: number;
        encrypt: (input: string, secret: string) => string;
        decrypt: (strInput: string, secret: string) => string;
    };
    gcm: {
        algorithm: string;
        authTagLength: number;
        ivLength: number;
        encrypt: (input: string, secret: string) => string;
        decrypt: (strInput: string, secret: string) => string;
    };
};
